# README #

Build app and javadoc:
	$ ./gradlew build
	
Run program (located in build/libs/):
	$ java -jar TableComponent-x.x.x.jar
	
Possible VM arguments:
	-Dlocale=en,US or -Dlocale=cs,CZ
	$ java -jar -Dlocale=en,US TableComponent-x.x.x.jar

### What is this repository for? ###

Demo project for working with CSV.
Demodata.csv is located here: src\main\resources\demodata.csv
