package main;

import org.junit.Test;

import starter.Starter;

/**
 * Here should be as many Unit tests as possible. 
 * (Of course not all in this one class, but in this package.)
 * e.g.: 
 * 		1) All utils Classes.
 * 		2) Translation class.
 * 		3) BaseInteractions flow functionality methods.
 * 		4) All Resolvers methods (user interactions functionalities).
 * And just everything, which could be tested. :-)
 * 
 * @author josef
 *
 */
public class MainTest {
    @Test public void testSomeLibraryMethod() {
        Starter classUnderTest = new Starter();
//        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }
}
