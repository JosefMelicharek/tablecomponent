package components;

import java.util.List;
import java.util.stream.Collectors;

import beans.BaseBean;

public class Table<T extends BaseBean> {

	private List<T> rows;

	public Table(List<T> rows) {
		this.rows = rows;
	}
	
	public List<T> getRows() {
		return rows;
	}
	
	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	@Override
	public String toString() {
		return "Table [rows=" + 
				rows.stream()
					.map(t -> t.toString())
					.collect(Collectors.joining(System.lineSeparator())) + 
				"]";
	}
}
