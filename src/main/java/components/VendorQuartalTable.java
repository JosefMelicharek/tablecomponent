package components;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import beans.VendorQuartal;

public class VendorQuartalTable extends Table<VendorQuartal> {

	public VendorQuartalTable(List<VendorQuartal> rows) {
		super(rows);
	}

	public VendorQuartalTable filterByVendor(String vendor) {
		return new VendorQuartalTable(
				getRows()
					.stream()
					.filter(r -> r.getVendor().equalsIgnoreCase(vendor))
					.collect(Collectors.toList())
				);
	}
	
	public VendorQuartalTable filterByCountry(String country) {
		return new VendorQuartalTable(
				getRows()
					.stream()
					.filter(r -> r.getCountry().equalsIgnoreCase(country))
					.collect(Collectors.toList())
				);
	}
	
	public VendorQuartalTable filterByQuartal(String quartal) {
		return new VendorQuartalTable(
				getRows()
					.stream()
					.filter(r -> r.getTimescale().equalsIgnoreCase(quartal))
					.collect(Collectors.toList())
				);
	}

	public VendorQuartalTable calculateSharePercent() {
		return new VendorQuartalTable(calculateShareRows());
	}

	private List<VendorQuartal> calculateShareRows() {
		List<VendorQuartal> result = new LinkedList<>(getRows());
		double unitsSum = result.stream().collect(Collectors.summingDouble(r -> r.getUnits()));
		result.stream().forEach(r -> r.setShare(100 * r.getUnits() / unitsSum));
		
		result.add(new VendorQuartal("Total", unitsSum, 100.0));
		return result;
	}

	public void sortByVendor() {
		getRows().sort((r1, r2) -> r1.getVendor().compareTo(r2.getVendor()));
		checkTotalRow();
	}

	public void sortByUnits() {
		getRows().sort((r1, r2) -> r1.getUnits().compareTo(r2.getUnits()));
		checkTotalRow();
	}
	
	/**
	 * Check if total row is at last position.
	 */
	private void checkTotalRow() {
		List<VendorQuartal> rows = getRows();
		int totalIndex = - 1;
		for (int i = 0; i < rows.size(); i++) {
			VendorQuartal row = rows.get(i);
			if (row.getVendor().equalsIgnoreCase("total")) {
				if (i != rows.size() - 1) {
					totalIndex = i;
					break;
				}
			}
		}
		
		if (totalIndex != -1) {
			VendorQuartal total = rows.remove(totalIndex);
			rows.add(total);
		}
	}

	public List<String[]> toStandardRows() {
		List<String[]> result = new LinkedList<>();
		
		result.add(new String[] {"Vendor", "Units", "Share"});
		for (VendorQuartal row : getRows()) {
			result.add(new String[] {
					row.getVendor(), 
					String.format(VendorQuartal.DEFAULT_UNITS_FORMAT, row.getUnits()), 
					String.format(VendorQuartal.DEFAULT_SHARE_FORMAT, row.getShare())
			});
		}
		return result;
	}
}
