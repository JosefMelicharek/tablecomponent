package logger;

import java.io.IOException;
import java.util.logging.Formatter;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * This class was inspired from here:
 * https://www.vogella.com/tutorials/Logging/article.html
 *
 */
public class MyLogger {
	
	private static final String LOGGING_TXT_FILE = "Logging.txt";
	private static final String LOGGING_HTML_FILE = "Logging.html";
	private static final String LOGGER_INITIALIZATION_FAILED_MESSAGE = "Logger initialization failed.";
	
	static {
		try {
			setup();
		} catch (IOException e) {
			System.out.println(LOGGER_INITIALIZATION_FAILED_MESSAGE);
			e.printStackTrace();
		}
	}

	// use the classname for the logger, this way you can refactor
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private static FileHandler fileTxt;
	private static SimpleFormatter formatterTxt;
	
	private static FileHandler fileHTML;
	private static Formatter formatterHTML;

	private static void setup() throws IOException {
        Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

        // suppress the logging output to the console
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            rootLogger.removeHandler(handlers[0]);
        }

        // could be set by java parameter
        logger.setLevel(Level.INFO);
        fileTxt = new FileHandler(LOGGING_TXT_FILE);
        fileHTML = new FileHandler(LOGGING_HTML_FILE);

        formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formatterTxt);
        logger.addHandler(fileTxt);

        formatterHTML = new MyHtmlFormatter();
        fileHTML.setFormatter(formatterHTML);
        logger.addHandler(fileHTML);
    }

	public static void severe(String message) {
		LOGGER.severe(message);
	}

	public static void warn(String message) {
		LOGGER.warning(message);
	}
}
