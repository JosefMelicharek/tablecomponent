package starter;

import data.VendorQuartalsDataProvider;
import data.StandardData;
import interactions.BaseInteractions;
import interactions.ChoiceType;
import interactions.ConsoleInteractions;
import interactions.UserInteractions;
import interactions.beans.UserChoice;
import interactions.resolvers.UserChoicesResolver;
import interactions.resolvers.UserChoicesResolverFactory;
import translations.Translations;
import utils.LocaleUtils;

public class Starter {
	
	public static final String LOCALE_PROPERTY_KEY = "locale";
	public static final String DEFAULT_LOCALE = "cs,CZ";
	
	public static void main(String[] args) throws Exception {
		// type of user interactions could be set by java parameter
		UserInteractions interactions = new ConsoleInteractions(System.in);
		String locale = System.getProperty(LOCALE_PROPERTY_KEY, DEFAULT_LOCALE);
		Translations.initializeInstance(LocaleUtils.fromString(locale));
		
		runProgram(interactions);
	}

	private static void runProgram(UserInteractions interactions) {
		Translations translations = Translations.getInstance();
		
		VendorQuartalsDataProvider data = new StandardData();
		
		interactions.message(translations.getMessage(Translations.WELCOME_MESSAGE));
		interactions.startCommunicateWithUser(BaseInteractions.DEFAULT_CHOICES);
		interactions.showOptions();
		
		boolean running = true;
		while (running) {
			UserChoice choice = interactions.waitForUserChoice();
			if (choice == null || choice.getType() == ChoiceType.END_PROGRAM) {
				interactions.message(translations.getMessage(Translations.PROGRAM_TERMINATED));
				running = false;
			} else {
				UserChoicesResolver resolver = UserChoicesResolverFactory.getResolverBy(choice, interactions, data);
				resolver.resolve();
				interactions.showOptions();
			}
		}
	}
}
