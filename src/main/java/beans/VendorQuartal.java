package beans;

import com.opencsv.bean.CsvBindByName;

public class VendorQuartal extends BaseBean {
	
	public static final String DEFAULT_UNITS_FORMAT = "%.3f";
	public static final String DEFAULT_SHARE_FORMAT = "%.1f%%";

	@CsvBindByName
	private String country;
	@CsvBindByName
	private String timescale;
	@CsvBindByName
	private String vendor;
	@CsvBindByName
	private Double units;
	
	private Double share;
	
	public VendorQuartal() {}
	
	public VendorQuartal(String vendor, Double units, Double share) {
		this.vendor = vendor;
		this.units = units;
		this.share = share;
	}

	public String getCountry() {
		return country;
	}
	
	public String getTimescale() {
		return timescale;
	}
	
	public String getVendor() {
		return vendor;
	}
	
	public Double getUnits() {
		return units;
	}
	
	public void setShare(Double share) {
		this.share = share;
	}
	
	public Double getShare() {
		return share;
	}

	@Override
	public String[] getColumnNames() {
		return new String[] {"country", "timescale", "vendor", "units"};
	}
	
	@Override
	public String[] getSimpleDataAsString() {
		return new String[] {country, timescale, vendor, String.valueOf(units)};
	}
	
	@Override
	public String toString() {
		return "VendorQuartal [country=" + country + ", timescale=" + timescale + ", vendor=" + vendor + ", units=" + units + "]";
	}
}
