package beans;

/**
 * Definition for easier reading of any bean. Not used yet.
 * @author josef
 *
 */
public abstract class BaseBean {
	
	public abstract String[] getColumnNames();
	public abstract String[] getSimpleDataAsString();
	
}
