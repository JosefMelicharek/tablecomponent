package translations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import interactions.ChoiceType;
import logger.MyLogger;
import utils.StringUtils;

/**
 * Singleton Class for localized messages to user.
 * @author josef
 *
 */
public class Translations {

	public static final String INSTANCE_WAS_NOT_INITIALIZED = "The instance was not initialized. Contact app creator.";
	public static final String COULD_NOT_FIND_TRANSLATION_MESSAGE = "Could not find translation: %s for locale: %s";
	public static final String LOCALE_IS_NOT_SUPPORTED_MESSAGE = "Locale %s not recognized. The default %s has been set.";
	public static final String NOT_LOCALIZED_MESSAGE = "-- Could not find translation, please contact app creator. --";
	
	public static final String WELCOME_MESSAGE = "welcomeMessage";
	public static final String ENTER_AN_OPTION = "enterAnOption";
	public static final String GO_BACK = "goBack";
	public static final String ENTER_INTEGER = "enterInteger";
	public static final String PROGRAM_TERMINATED = "programTerminated";
	public static final String FILE_PATH = "filePath";
	public static final String EXCEPTION_IN_IO = "exceptionInIO";
	public static final String LOAD_CSV_FIRST = "loadCsvFirst";
	public static final String COUNT_SHARE_FIRST = "countShareFirst";
	public static final String ENTER_VENDOR = "enterVendor";
	public static final String ENTER_COUNTRY = "enterCountry";
	public static final String ENTER_QUARTAL = "enterQuartal";
	public static final String ENTER_COLUMN = "enterColumn";
	public static final String LAST_OPERATION = "lastOperation";
	public static final String NOT_IMPLEMENTED_YET = "notImplementedYet";
	public static final String OUTPUT_FILE_NAME = "outputFileName";
	public static final String INPUT_ERROR = "inputError";
	public static final String VENDOR_NOT_FOUND = "vendorNotFound";

	private static final List<Locale> SUPPORTED_LOCALES = Arrays.asList(
			new Locale("cs", "CZ"), 
			new Locale("en", "US"));

	private static final Locale CZ = SUPPORTED_LOCALES.get(0);
	private static final Locale EN = SUPPORTED_LOCALES.get(1);
	
	private static Translations instance;
	private Locale locale;
	private Map<String, Map<Locale, String>> messagesKeysToTranslations;
	
	public static void initializeInstance(Locale locale) {
		instance = new Translations(locale);
	}
	
	public static Translations getInstance() {
		if (instance == null) {
			MyLogger.severe(INSTANCE_WAS_NOT_INITIALIZED);
			instance = new Translations(CZ);
		}
		return instance;
	}

	
	private Translations(Locale locale) {
		if (!SUPPORTED_LOCALES.contains(locale)) {
			MyLogger.warn(String.format(LOCALE_IS_NOT_SUPPORTED_MESSAGE, locale, EN));
			locale = CZ;
		}
		this.locale = locale;
		initializeTranslations();
	}

	/**
	 * Temp functionality. Translations should be in some .properties files and not in one big Map.
	 */
	private void initializeTranslations() {
		this.messagesKeysToTranslations = new HashMap<>();

		this.messagesKeysToTranslations.put(WELCOME_MESSAGE, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Vítejte v aplikaci práce s CSV soubory.");
				put(EN, "Welcome in application for working with CSV files.");
			}
		});
		this.messagesKeysToTranslations.put(ENTER_AN_OPTION, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zvolte volbu:");
				put(EN, "Enter an option:");
			}
		});
		this.messagesKeysToTranslations.put(GO_BACK, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zpět");
				put(EN, "Back");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.END_PROGRAM.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Ukončit program");
				put(EN, "Exit program");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.CSV_OPERATIONS.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "CSV Operace");
				put(EN, "CSV Operations");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.LOAD_CSV.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Nahrání souboru");
				put(EN, "Load file");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.SHOW_TABLE.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zobraz tabulku");
				put(EN, "Show table");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.SORT_TABLE.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Seřaď tabulku");
				put(EN, "Sort table");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.EXPORT_TABLE.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Exportuj");
				put(EN, "Export");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.CSV_EXPORT.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "CSV");
				put(EN, "CSV");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.EXCEL_EXPORT.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Excel");
				put(EN, "Excel");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.HTML_EXPORT.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "HTML");
				put(EN, "HTML");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.UNITS_AND_SHARE.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Jednotky a podíl");
				put(EN, "Units and share");
			}
		});
		this.messagesKeysToTranslations.put(ChoiceType.ROW_NUMBER.toString(), new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Číslo řádku obchodníka");
				put(EN, "Vendor row number");
			}
		});
		this.messagesKeysToTranslations.put(ENTER_INTEGER, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte celé číslo z rozsahu voleb");
				put(EN, "Enter an integer from the range of options");
			}
		});
		this.messagesKeysToTranslations.put(PROGRAM_TERMINATED, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Program ukončen.");
				put(EN, "Program terminated.");
			}
		});
		this.messagesKeysToTranslations.put(FILE_PATH, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte cestu s názvem souboru.");
				put(EN, "Enter the path with the file name.");
			}
		});
		this.messagesKeysToTranslations.put(EXCEPTION_IN_IO, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Chyba vstupu / výstupu:");
				put(EN, "Input / output error:");
			}
		});
		this.messagesKeysToTranslations.put(LOAD_CSV_FIRST, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Nejdříve načtěte CSV soubor.");
				put(EN, "Load CSV file first.");
			}
		});
		this.messagesKeysToTranslations.put(ENTER_VENDOR, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte obchodníka");
				put(EN, "Enter vendor");
			}
		});
		this.messagesKeysToTranslations.put(ENTER_COUNTRY, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte zemi");
				put(EN, "Enter country");
			}
		});
		this.messagesKeysToTranslations.put(ENTER_QUARTAL, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte rok a kvartál");
				put(EN, "Enter year and quartal");
			}
		});
		this.messagesKeysToTranslations.put(ENTER_COLUMN, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte sloupec, možné hodnoty: ");
				put(EN, "Enter column, possible values: ");
			}
		});
		this.messagesKeysToTranslations.put(COUNT_SHARE_FIRST, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Nejdříve spočtěte jednotky a podíl.");
				put(EN, "First, count the units and share.");
			}
		});
		this.messagesKeysToTranslations.put(LAST_OPERATION, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Tabulka poslední operace:");
				put(EN, "Table of last operation:");
			}
		});
		this.messagesKeysToTranslations.put(LAST_OPERATION, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Funkce prozatím nebyla implementována.");
				put(EN, "Feature not implemented yet.");
			}
		});
		this.messagesKeysToTranslations.put(OUTPUT_FILE_NAME, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Zadejte jméno výstupního souboru:");
				put(EN, "Set output file name:");
			}
		});
		this.messagesKeysToTranslations.put(INPUT_ERROR, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Chyba vstupních dat.");
				put(EN, "Input data error.");
			}
		});
		this.messagesKeysToTranslations.put(NOT_IMPLEMENTED_YET, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Funkce ještě nebyla implementována.");
				put(EN, "The feature has not yet been implemented.");
			}
		});
		this.messagesKeysToTranslations.put(VENDOR_NOT_FOUND, new HashMap<Locale, String>() {
			private static final long serialVersionUID = 1L;
			{
				put(CZ, "Obchodník nebyl nalezen.");
				put(EN, "Vendor not found.");
			}
		});
	}

	public String getMessage(String messageKey) {
		Map<Locale, String> translations = messagesKeysToTranslations.get(messageKey);
		if (translations == null) {
			MyLogger.severe(String.format(COULD_NOT_FIND_TRANSLATION_MESSAGE, messageKey, locale));
			return NOT_LOCALIZED_MESSAGE;
		} else {
			return getLocalizedMessage(translations);
		}
	}

	private String getLocalizedMessage(Map<Locale, String> translations) {
		String translation = translations.get(locale);
		if (StringUtils.isEmpty(translation) && !translations.isEmpty()) {
			// TODO Here should be a selection by priority in case of more languages support.
			return translations.values().iterator().next();
		}
		return translation;
	}

}
