package utils;

/**
 * Rather use apache.commons instead of this Class in the future.
 * @author josef
 *
 */
public class StringUtils {

	public static boolean isEmpty(String string) {
		return string == null || string.isEmpty();
	}

}
