package utils;

import java.util.Locale;
import java.util.StringTokenizer;

public class LocaleUtils {

	public String localeToString(Locale l) {
		return l.getLanguage() + "," + l.getCountry();
	}

	public static Locale fromString(String locale) {
		StringTokenizer tempStringTokenizer = new StringTokenizer(locale, ",");
		String language = "";
		if (tempStringTokenizer.hasMoreTokens()) {
			language = tempStringTokenizer.nextElement().toString();
		}
		String country = "";
		if (tempStringTokenizer.hasMoreTokens()) {
			country = tempStringTokenizer.nextElement().toString();
		}
		return new Locale(language, country);
	}

}
