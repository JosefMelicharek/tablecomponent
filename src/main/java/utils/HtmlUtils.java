package utils;

import java.util.List;

public class HtmlUtils {
	
	private static final String TABLE_HEAD_TEMPLATE = "<th style=\"background-color: lightgray;\"> %s </th>";
	private static final String TABLE_COLUMN_TEMPLATE = "<td style=\"text-align: center; width: 200px;\"> %s </td>";

	public static String generateTable(List<String[]> rows) {
		StringBuilder sb = new StringBuilder();
		
		createHeader(sb);

		sb.append("<table border=\"1\">");
		createTableHeader(sb, rows.remove(0));
		for (int i = 0; i < rows.size(); i++) {
			String[] row = rows.get(i);
			if (i == rows.size() - 1) {
				sb.append("<tr style=\"background-color: lightyellow;\">");
			} else {
				sb.append("<tr>");
			}
		    for (String column : row) {
		    	sb.append(String.format(TABLE_COLUMN_TEMPLATE, column));
		    }
		    sb.append("</tr>");
		}
		sb.append("</table>");
		
		createFooter(sb);
		
		return sb.toString();
	}

	private static void createHeader(StringBuilder sb) {
		sb.append("<html>");
		sb.append("<head>");
		sb.append("</head>");
	}

	private static void createFooter(StringBuilder sb) {
		sb.append("</body>");
		sb.append("</html>");
	}
	
	private static void createTableHeader(StringBuilder sb, String[] columns) {
		for (String column : columns) {
			sb.append(String.format(TABLE_HEAD_TEMPLATE, column));
		}
	}

}
