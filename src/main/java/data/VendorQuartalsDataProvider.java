package data;

import components.VendorQuartalTable;

/**
 * Define base data space for working with VendorQuartals.
 * 
 * setTable and getTable should reference input table from file.
 * setGroupedTable and getGroupedTable reference counted "Units and Share" table.
 * @author josef
 *
 */
public interface VendorQuartalsDataProvider {

	void setTable(VendorQuartalTable table);
	
	VendorQuartalTable getTable();

	void setGroupedTable(VendorQuartalTable grouppedByVendor);
	
	VendorQuartalTable getGroupedTable();
	
}
