package data;

import components.VendorQuartalTable;

public class StandardData implements VendorQuartalsDataProvider {
	
	private VendorQuartalTable table;
	private VendorQuartalTable grouppedTable;

	@Override
	public void setTable(VendorQuartalTable table) {
		this.table = table;
	}
	
	@Override
	public VendorQuartalTable getTable() {
		return table;
	}

	@Override
	public void setGroupedTable(VendorQuartalTable grouppedTable) {
		this.grouppedTable = grouppedTable;
	}

	@Override
	public VendorQuartalTable getGroupedTable() {
		return grouppedTable;
	}

}
