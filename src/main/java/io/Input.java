package io;

import java.io.IOException;
import java.util.List;

import com.opencsv.exceptions.CsvException;

import beans.BaseBean;

public interface Input {

	List<String[]> loadCSV(char separator) throws IOException, CsvException;

	<T extends BaseBean> List<T> loadCSVBeans(Class<T> type, char separator) throws IOException;

}
