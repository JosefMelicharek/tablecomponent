package io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.exceptions.CsvException;

import beans.BaseBean;

public class LocalFilesystemInput implements Input {
	
	private String fileName;

	public LocalFilesystemInput(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public List<String[]> loadCSV(char separator) throws IOException, CsvException {
		Path myPath = Paths.get(fileName);
		CSVParser parser = new CSVParserBuilder()
				.withSeparator(separator)
				.build();
		
		try (BufferedReader br = Files.newBufferedReader(myPath, StandardCharsets.UTF_8);
				CSVReader reader = new CSVReaderBuilder(br).withCSVParser(parser).build()) {
			
			return reader.readAll();
		}
	}

	@Override
	public <T extends BaseBean> List<T> loadCSVBeans(Class<T> type, char separator) throws IOException {
		Path myPath = Paths.get(fileName);
		try (BufferedReader br = Files.newBufferedReader(myPath, StandardCharsets.UTF_8)) {
            HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(type);

            CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSeparator(separator)
                    .build();

            return csvToBean.parse();
        }
	}
}
