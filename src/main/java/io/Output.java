package io;

import java.io.IOException;

public interface Output {

	void toHtml(String htmlText) throws IOException;

}
