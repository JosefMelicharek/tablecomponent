package io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LocalFilesystemOutput implements Output {
	
	private static final String HTML_SUFFIX = ".html";
	
	private String fileName;

	public LocalFilesystemOutput(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void toHtml(String htmlText) throws IOException {
		if (!fileName.toLowerCase().endsWith(HTML_SUFFIX)) {
			fileName = fileName.concat(HTML_SUFFIX);
		}
		Files.write(Paths.get(fileName), htmlText.getBytes());
	}

}
