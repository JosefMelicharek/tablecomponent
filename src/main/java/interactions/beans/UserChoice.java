package interactions.beans;

import java.util.List;

import interactions.ChoiceType;

public class UserChoice {
	
	private final ChoiceType type;
	private final List<UserChoice> parent;
	private final List<UserChoice> children;
	
	public UserChoice(ChoiceType type, List<UserChoice> parent, List<UserChoice> children) {
		this.type = type;
		this.parent = parent;
		this.children = children;
	}

	public ChoiceType getType() {
		return type;
	}

	public List<UserChoice> getParent() {
		return parent;
	}

	public List<UserChoice> getChildren() {
		return children;
	}
	
	public boolean hasParent() {
		return parent != null;
	}
	
	public boolean hasChildren() {
		return children != null;
	}

}
