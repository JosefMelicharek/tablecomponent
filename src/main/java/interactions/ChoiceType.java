package interactions;

/**
 * All interactions which user can do.
 * @author josef
 *
 */
public enum ChoiceType {
	
	END_PROGRAM, 
	CSV_OPERATIONS,
	
	LOAD_CSV, 
	SHOW_TABLE, 
	UNITS_AND_SHARE,
	ROW_NUMBER,
	SORT_TABLE, 
	EXPORT_TABLE,
	
	CSV_EXPORT, 
	EXCEL_EXPORT, 
	HTML_EXPORT;
}
