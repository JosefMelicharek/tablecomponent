package interactions;

import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class InputStreamInteractions extends BaseInteractions implements UserInteractions {

	private Scanner scanner;

	public InputStreamInteractions(InputStream in) {
		this.scanner = new Scanner(in);
	}
	
	@Override
	protected int getChoiceIndex() {
		Integer index = null;
		while (index == null) {
			try {
				index = scanner.nextInt();
				scanner.nextLine();
			} catch (InputMismatchException e) {
				badOption();
				// nextLine as flush here - not best option
				scanner.nextLine();
			}
		}
		return index;
	}
	
	@Override
	public String waitForInputString() {
		return scanner.nextLine();
	}

}
