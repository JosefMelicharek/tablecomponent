package interactions.resolvers;

import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import translations.Translations;

public class RowNumberResolver extends UserChoicesResolver {
	
	// should be localized
	public static final String RESULT_TEMPLATE = "The %d. row.";

	public RowNumberResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		super(interactions, data);
	}
	
	@Override
	public void resolve() {
		Translations translations = Translations.getInstance();
		VendorQuartalTable table = getData().getGroupedTable();
		if (checkDataExists(table, Translations.COUNT_SHARE_FIRST)) {
			getInteractions().message(translations.getMessage(Translations.ENTER_VENDOR));
			String vendor = getInteractions().waitForInputString();
			
			getInteractions().message(showVendorRowIndex(table, vendor));
		}
	}

	private String showVendorRowIndex(VendorQuartalTable table, String vendor) {
		int index = -1;
		for (int i = 0; i < table.getRows().size(); i++) {
			// could be more rows then one - in case of bad input file
			if (table.getRows().get(i).getVendor().equalsIgnoreCase(vendor)) {
				index = i;
				break;
			}
		}
		if (index != -1) {			
			return String.format(RESULT_TEMPLATE, index + 1);
		}
		return Translations.getInstance().getMessage(Translations.VENDOR_NOT_FOUND);
	}

}
