package interactions.resolvers;

import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import translations.Translations;

public class SortTableResolver extends UserChoicesResolver {
	
	public static final String VENDOR = "vendor";
	public static final String UNITS = "units";

	public SortTableResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		super(interactions, data);
	}
	
	@Override
	public void resolve() {
		Translations translations = Translations.getInstance();
		VendorQuartalTable table = getData().getTable();
		if (checkDataExists(table, Translations.LOAD_CSV_FIRST)) {
			getInteractions().message(translations.getMessage(Translations.ENTER_COLUMN) + String.format(" (%s, %s)", VENDOR, UNITS));
			String column = getInteractions().waitForInputString();
			if (column.equalsIgnoreCase(VENDOR)) {
				getData().getTable().sortByVendor();
				
				if (getData().getGroupedTable() != null) {
					getData().getGroupedTable().sortByVendor();					
				}
				
				// should be check of exact type, or notify user
			} else {
				getData().getTable().sortByUnits();
				
				if (getData().getGroupedTable() != null) {
					getData().getGroupedTable().sortByUnits();				
				}
			}
		}
	}

}
