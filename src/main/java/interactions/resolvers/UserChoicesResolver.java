package interactions.resolvers;

import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import translations.Translations;

/**
 * Base functionalities for all User Choices Resolvers.
 * @author josef
 *
 */
public abstract class UserChoicesResolver {
	
	private UserInteractions interactions;
	private VendorQuartalsDataProvider data;

	public UserChoicesResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		this.interactions = interactions;
		this.data = data;
	}
	
	public UserInteractions getInteractions() {
		return interactions;
	}
	
	public VendorQuartalsDataProvider getData() {
		return data;
	}
	
	protected boolean checkDataExists(VendorQuartalTable table, String translationKey) {
		if (table == null) {
			interactions.message(Translations.getInstance().getMessage(translationKey));
			return false;
		}
		return true;
	}

	public abstract void resolve();
}
