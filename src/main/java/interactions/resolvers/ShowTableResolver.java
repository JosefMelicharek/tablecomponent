package interactions.resolvers;

import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import translations.Translations;

/**
 * Prints all tables from memory if they exist.
 * @author josef
 *
 */
public class ShowTableResolver extends UserChoicesResolver {

	public ShowTableResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		super(interactions, data);
	}
	
	@Override
	public void resolve() {
		VendorQuartalTable table = getData().getTable();
		if (checkDataExists(table, Translations.LOAD_CSV_FIRST)) {
			getInteractions().message(table.toString());
			
			if (getData().getGroupedTable() != null) {
				getInteractions().message(Translations.getInstance().getMessage(Translations.LAST_OPERATION));
				getInteractions().message(getData().getGroupedTable().toString());
			}
		}
	}

}
