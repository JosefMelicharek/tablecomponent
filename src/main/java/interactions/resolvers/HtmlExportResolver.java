package interactions.resolvers;

import java.io.IOException;

import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import io.LocalFilesystemOutput;
import io.Output;
import translations.Translations;
import utils.HtmlUtils;

public class HtmlExportResolver extends UserChoicesResolver {

	public HtmlExportResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		super(interactions, data);
	}
	
	@Override
	public void resolve() {
		VendorQuartalTable table = getData().getGroupedTable();
		if (checkDataExists(table, Translations.COUNT_SHARE_FIRST)) {
			getInteractions().message(Translations.getInstance().getMessage(Translations.OUTPUT_FILE_NAME));
			String outputFile = getInteractions().waitForInputString();
			
			Output output = new LocalFilesystemOutput(outputFile);
			try {
				output.toHtml(HtmlUtils.generateTable(table.toStandardRows()));
			} catch (IOException e) {
				getInteractions().message(Translations.EXCEPTION_IN_IO);
				getInteractions().message(e.getLocalizedMessage());
			}
		}
	}

}
