package interactions.resolvers;

import beans.VendorQuartal;
import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import translations.Translations;

public class UnitsAndShareResolver extends UserChoicesResolver {
	
	// should be localized
	public static final String RESULT_TEMPLATE = VendorQuartal.DEFAULT_UNITS_FORMAT + " and " + VendorQuartal.DEFAULT_SHARE_FORMAT;

	public UnitsAndShareResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		super(interactions, data);
	}
	
	@Override
	public void resolve() {
		Translations translations = Translations.getInstance();
		VendorQuartalTable table = getData().getTable();
		if (checkDataExists(table, Translations.LOAD_CSV_FIRST)) {
			getInteractions().message(translations.getMessage(Translations.ENTER_VENDOR));
			String vendor = getInteractions().waitForInputString();
			getInteractions().message(translations.getMessage(Translations.ENTER_COUNTRY));
			String country = getInteractions().waitForInputString();
			getInteractions().message(translations.getMessage(Translations.ENTER_QUARTAL));
			String quartal = getInteractions().waitForInputString();
			
			getInteractions().message(calculateUnitsAndShare(table, vendor, country, quartal));
		}
	}

	private String calculateUnitsAndShare(VendorQuartalTable table, String vendor, String country, String quartal) {
		VendorQuartalTable quartalTable = table.filterByQuartal(quartal).filterByCountry(country);
		VendorQuartalTable groupedByVendor = quartalTable.calculateSharePercent();
		getData().setGroupedTable(groupedByVendor);
		VendorQuartalTable result = groupedByVendor.filterByVendor(vendor);
		
		// TODO could be more rows - depends on input file
		if (!result.getRows().isEmpty()) {
			VendorQuartal firstRow = result.getRows().get(0);
			return String.format(RESULT_TEMPLATE, firstRow.getUnits(), firstRow.getShare());
		} else {
			return Translations.getInstance().getMessage(Translations.INPUT_ERROR);
		}
	}

}
