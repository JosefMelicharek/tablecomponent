package interactions.resolvers;

import java.io.IOException;
import java.util.List;

import beans.VendorQuartal;
import components.VendorQuartalTable;
import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import io.Input;
import io.LocalFilesystemInput;
import translations.Translations;

public class LoadCsvResolver extends UserChoicesResolver {

	public LoadCsvResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
		super(interactions, data);
	}
	
	@Override
	public void resolve() {
		getInteractions().message(Translations.getInstance().getMessage(Translations.FILE_PATH));
		String filePath = getInteractions().waitForInputString();
		
		Input input = new LocalFilesystemInput(filePath);
		try {
			// delimiter should be as next input from user
			List<VendorQuartal> vendorQuartals = input.loadCSVBeans(VendorQuartal.class, ',');
			getData().setTable(new VendorQuartalTable(vendorQuartals));
		} catch (IOException e) {
			getInteractions().message(Translations.EXCEPTION_IN_IO);
			getInteractions().message(e.getLocalizedMessage());
		}
	}

}
