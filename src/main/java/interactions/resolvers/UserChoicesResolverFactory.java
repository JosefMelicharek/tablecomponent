package interactions.resolvers;

import data.VendorQuartalsDataProvider;
import interactions.UserInteractions;
import interactions.beans.UserChoice;
import translations.Translations;

/**
 * Factory class for creating UserChoicesResolvers instances.
 * @author josef
 *
 */
public class UserChoicesResolverFactory {

	public static UserChoicesResolver getResolverBy(UserChoice choice, UserInteractions interactions,
			VendorQuartalsDataProvider data) {
		switch (choice.getType()) {
		case LOAD_CSV:
			return new LoadCsvResolver(interactions, data);
		case SHOW_TABLE:
			return new ShowTableResolver(interactions, data);
		case UNITS_AND_SHARE:
			return new UnitsAndShareResolver(interactions, data);
		case ROW_NUMBER:
			return new RowNumberResolver(interactions, data);
		case SORT_TABLE:
			return new SortTableResolver(interactions, data);
		case HTML_EXPORT:
			return new HtmlExportResolver(interactions, data);
		case CSV_EXPORT:
		case EXCEL_EXPORT:
			return new TempResolver(interactions, data);
		default:
			throw new UnsupportedOperationException("Unknown choice.");
		}
	}
	
	private static class TempResolver extends UserChoicesResolver {
		public TempResolver(UserInteractions interactions, VendorQuartalsDataProvider data) {
			super(interactions, data);
		}

		@Override
		public void resolve() {
			getInteractions().message(Translations.getInstance().getMessage(Translations.NOT_IMPLEMENTED_YET));
		}
	}

}
