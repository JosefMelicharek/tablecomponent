package interactions;

import java.util.LinkedList;
import java.util.List;

import interactions.beans.UserChoice;
import translations.Translations;

/**
 * This class wraps the basic flow of user interactions.
 * 
 * One big functionality is missing here:
 * 		Some choices should be visible only in some cases. Not all the times.
 * 		e.g.: If the file has not been uploaded yet, the option to export should not be displayed.
 * @author josef
 *
 */
public abstract class BaseInteractions implements UserInteractions {

	public static final String OPTION_TEMPLATE = "%d - %s; ";

	public static final List<UserChoice> DEFAULT_CHOICES;
	// Default interactions initialization, ideally should be in some resources/json file.
	static {
		DEFAULT_CHOICES = new LinkedList<>();
		
		List<UserChoice> csvOperationsChildren = new LinkedList<>();
		List<UserChoice> exportTableChildren = new LinkedList<>();
		
		exportTableChildren.add(new UserChoice(ChoiceType.CSV_EXPORT, csvOperationsChildren, null));
		exportTableChildren.add(new UserChoice(ChoiceType.EXCEL_EXPORT, csvOperationsChildren, null));
		exportTableChildren.add(new UserChoice(ChoiceType.HTML_EXPORT, csvOperationsChildren, null));

		csvOperationsChildren.add(new UserChoice(ChoiceType.LOAD_CSV, DEFAULT_CHOICES, null));
		csvOperationsChildren.add(new UserChoice(ChoiceType.SHOW_TABLE, DEFAULT_CHOICES, null));
		csvOperationsChildren.add(new UserChoice(ChoiceType.UNITS_AND_SHARE, DEFAULT_CHOICES, null));
		csvOperationsChildren.add(new UserChoice(ChoiceType.ROW_NUMBER, DEFAULT_CHOICES, null));
		csvOperationsChildren.add(new UserChoice(ChoiceType.SORT_TABLE, DEFAULT_CHOICES, null));
		csvOperationsChildren.add(new UserChoice(ChoiceType.EXPORT_TABLE, DEFAULT_CHOICES, exportTableChildren));

		UserChoice csvOperations = new UserChoice(ChoiceType.CSV_OPERATIONS, null, csvOperationsChildren);
		UserChoice endProgram = new UserChoice(ChoiceType.END_PROGRAM, null, null);

		DEFAULT_CHOICES.add(endProgram);
		DEFAULT_CHOICES.add(csvOperations);
	}

	private List<UserChoice> actualChoices;

	@Override
	public void startCommunicateWithUser(List<UserChoice> choices) {
		this.actualChoices = choices;
	}

	/**
	 * Shows possible paths from the current part of the program. 
	 * 0 - GoBack, 1 - Import file, ...
	 */
	@Override
	public void showOptions() {
		StringBuilder sb = new StringBuilder();
		Translations translations = Translations.getInstance();

		sb.append(translations.getMessage(Translations.ENTER_AN_OPTION));
		sb.append(System.lineSeparator());
		for (int i = 0; i < actualChoices.size(); i++) {
			UserChoice choice = actualChoices.get(i);
			if (i == 0 && choice.hasParent()) {
				sb.append(generateOption(i, Translations.GO_BACK));
			}
			sb.append(generateOption(i + 1, choice.getType().toString()));
		}
		sb.append(System.lineSeparator());

		message(sb.toString());
	}

	private String generateOption(int i, String translationKey) {
		return String.format(OPTION_TEMPLATE, i, Translations.getInstance().getMessage(translationKey));
	}

	/**
	 * Returns UserChoice instance or null in case of program exit.
	 */
	@Override
	public UserChoice waitForUserChoice() {
		while (true) {
			int index = getChoiceIndex();
			if (index < 0 || index > actualChoices.size()) {
				badOption();
			} else {
				UserChoice choice = getChoice(index);
				return processChoice(choice);
			}
		}
	}
	
	private UserChoice getChoice(int index) {
		if (index == 0) {
			return null;
		}
		return actualChoices.get(index - 1);
	}

	/**
	 * Main navigation logic in user interactions.
	 * @param choice - If it is null then the user goes back or terminates the program.
	 * 				 - If choice has children, Let the user search among the children options.
	 * 			     - If the option has no children, it is an end function that returns.
	 * @return Current choice, or null when goes back.
	 */
	private UserChoice processChoice(UserChoice choice) {
		if (choice == null) {
			List<UserChoice> choices = actualChoices.get(0).getParent();
			if (choices == null) {
				return null;
			} else {
				this.actualChoices = choices;
				showOptions();
				return waitForUserChoice();
			}
		}
		if (choice.hasChildren()) {
			this.actualChoices = choice.getChildren();
			showOptions();
			return waitForUserChoice();
		} else {
			return choice;
		}
	}

	protected void badOption() {
		message(Translations.getInstance().getMessage(Translations.ENTER_INTEGER));
	}

	protected abstract int getChoiceIndex();

}
