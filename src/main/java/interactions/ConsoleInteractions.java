package interactions;

import java.io.InputStream;

public class ConsoleInteractions extends InputStreamInteractions {
	
	public ConsoleInteractions(InputStream in) {
		super(in);
	}

	@Override
	public void message(String message) {
		System.out.println(message);
	}

}
