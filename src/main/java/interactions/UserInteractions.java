package interactions;

import java.util.List;

import interactions.beans.UserChoice;

/**
 * Interface for unified access to different types of communication with the user. (Console, GUI, TCP/IP, ...)
 * @author josef
 *
 */
public interface UserInteractions {

	void message(String message);

	void startCommunicateWithUser(List<UserChoice> interactions);

	void showOptions();

	UserChoice waitForUserChoice();

	String waitForInputString();

}
